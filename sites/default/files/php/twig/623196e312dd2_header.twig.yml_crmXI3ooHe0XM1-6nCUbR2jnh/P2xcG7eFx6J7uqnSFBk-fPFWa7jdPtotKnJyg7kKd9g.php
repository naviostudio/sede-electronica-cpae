<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/ssftheme/templates/includes/header.twig.yml */
class __TwigTemplate_e8e25e57971483707d2a3ffe1efa5289bfe60191a97e70c99fb3a901e883d419 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Site header"));
        echo "\">
    <nav class=\"navbar fixed-top navbar-expand-lg navbar-gov-co\">
        <div class=\"navbar-gov-co-pri container-fluid\">
            <div class=\"container\">
                <div class=\"navbar-logo float-left\">
                    <a class=\"navbar-brand\" href=\"https://www.gov.co/home/\" target=\"_blank\">
                        <img src=\"https://cdn.www.gov.co/assets/images/logo.png\" height=\"30\" alt=\"Logo Gov.co\"/>
                    </a>
                    <button
                            class=\"navbar-toggler\"
                            type=\"button\"
                            data-toggle=\"collapse\"
                            data-target=\"#navbarCollapsible\"
                            aria-controls=\"navbarCollapsible\"
                            aria-expanded=\"false\"
                            aria-label=\"Toggle navigation\"
                    >
                        <span class=\"navbar-toggler-icon\"></span>
                    </button>
                </div>
                <div class=\"collapse navbar-collapse navbar-first-menu float-right\">
                    <div class=\"nav-primary mx-auto\">
                        <ul class=\"navbar-nav ml-auto nav-items nav-items-desktop\"></ul>
                    </div>
                    <div class=\"nav-item-primary ml-auto mr-2\">
                        <div class=\"ssf-govco\">
                            <a href=\"https://www.gov.co/home/\" target=\"_blank\">Ir a GovCo</a>
                        </div>
                        <div class=\"ssf-gt\">";
        // line 29
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "gtranslate", [], "any", false, false, true, 29), 29, $this->source), "html", null, true);
        echo "</div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"navbar-nav navbar-notifications\" id=\"notificationHeader\"></div>
    </nav>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-9 ssf-logo\">
                <a href=\"";
        // line 39
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("<front>"));
        echo "\" rel=\"home\" class=\"site-branding__logo\">
                    <img src=\"";
        // line 40
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("<front>"));
        echo "sites/default/files/logo.png\" alt=\"";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Home"));
        echo "\"
                         class=\"img-fluid\"/>
                </a>
            </div>
            <div class=\"col-md-3\">
                <div class=\"content-search\">
                    <div class=\"form-group gov-co-form-group form-group-search\">
                        ";
        // line 47
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "form", [], "any", false, false, true, 47), 47, $this->source), "html", null, true);
        echo "
                    </div>
                </div>
            </div>
        </div>

    </div>
    <nav class=\"navbar navbar-gov-co-sedes \">
        <div class=\"menu-ssf\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        ";
        // line 59
        if ( !twig_get_attribute($this->env, $this->source, ($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method", false, false, true, 59)) {
            // line 60
            echo "                        <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null), 60, $this->source), "html", null, true);
            echo "\">
                            ";
        }
        // line 62
        echo "                            ";
        // line 63
        echo "                            ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation_collapsible", [], "any", false, false, true, 63)) {
            echo "    
                                <div>
                                    ";
            // line 65
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation_collapsible", [], "any", false, false, true, 65), 65, $this->source), "html", null, true);
            echo "
                                </div>
                            ";
        }
        // line 68
        echo "                            ";
        if ( !twig_get_attribute($this->env, $this->source, ($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method", false, false, true, 68)) {
            // line 69
            echo "                        </div>
                        ";
        }
        // line 71
        echo "                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>

";
    }

    public function getTemplateName()
    {
        return "themes/custom/ssftheme/templates/includes/header.twig.yml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 71,  140 => 69,  137 => 68,  131 => 65,  125 => 63,  123 => 62,  117 => 60,  115 => 59,  100 => 47,  88 => 40,  84 => 39,  71 => 29,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/ssftheme/templates/includes/header.twig.yml", "/var/www/drupal-9.3.0/themes/custom/ssftheme/templates/includes/header.twig.yml");
    }
    
    public function checkSecurity()
    {
        static $tags = array("if" => 59);
        static $filters = array("t" => 1, "escape" => 29);
        static $functions = array("path" => 39);

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['t', 'escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
