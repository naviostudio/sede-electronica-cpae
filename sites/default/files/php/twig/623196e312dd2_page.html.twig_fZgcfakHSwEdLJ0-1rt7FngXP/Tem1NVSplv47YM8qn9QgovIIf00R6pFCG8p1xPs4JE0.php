<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/ssftheme/templates/system/page.html.twig */
class __TwigTemplate_f33bb03d8b8045948485c3a9a7be5d058b1533b45012ea88f06f36228e57aac9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'main' => [$this, 'block_main'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
            'footer' => [$this, 'block_footer'],
            'copy' => [$this, 'block_copy'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->loadTemplate((($context["directory"] ?? null) . "/templates/includes/header.twig.yml"), "themes/custom/ssftheme/templates/system/page.html.twig", 1)->display($context);
        // line 2
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "title", [], "any", false, false, true, 2)) {
            // line 3
            echo "    ";
            $this->displayBlock('title', $context, $blocks);
        }
        // line 11
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "breadcrumb", [], "any", false, false, true, 11)) {
            // line 12
            echo "    ";
            $this->displayBlock('breadcrumb', $context, $blocks);
        }
        // line 20
        echo "<div class=\"container\">
    ";
        // line 22
        echo "    ";
        $this->displayBlock('main', $context, $blocks);
        // line 74
        echo "</div>

<div>
    <div class=\"container\">
        ";
        // line 78
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content_bottom1", [], "any", false, false, true, 78), 78, $this->source), "html", null, true);
        echo "
    </div>

</div>

<div class=\"content_bottom ";
        // line 83
        if (($context["front_page"] ?? null)) {
            echo " highlighted--home front-page ";
        }
        echo "\">
    <div class=\"container\">
        ";
        // line 85
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content_bottom", [], "any", false, false, true, 85), 85, $this->source), "html", null, true);
        echo "
    </div>
</div>


";
        // line 90
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 90)) {
            // line 91
            echo "    ";
            $this->displayBlock('footer', $context, $blocks);
        }
        // line 102
        echo "
";
        // line 103
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "copy", [], "any", false, false, true, 103)) {
            // line 104
            echo "    ";
            $this->displayBlock('copy', $context, $blocks);
        }
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "        <div class=\"container-fluid titulo--banner\">
            <div class=\"container contenedor-titulo\">
                ";
        // line 6
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "title", [], "any", false, false, true, 6), 6, $this->source), "html", null, true);
        echo "
            </div>
        </div>
    ";
    }

    // line 12
    public function block_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "        <div class=\"container-fluid ssf-bread\">
            <div class=\"container ssf-bread\">
                ";
        // line 15
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "breadcrumb", [], "any", false, false, true, 15), 15, $this->source), "html", null, true);
        echo "
            </div>
        </div>
    ";
    }

    // line 22
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "        <div role=\"main\" class=\"main-container ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null), 23, $this->source), "html", null, true);
        echo " js-quickedit-main-content\">
            <div class=\"row\">
                ";
        // line 26
        echo "                ";
        $context["content_classes"] = [0 => (((twig_get_attribute($this->env, $this->source,         // line 27
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 27) && twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 27))) ? ("col-sm-6") : ("")), 1 => (((twig_get_attribute($this->env, $this->source,         // line 28
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 28) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 28)))) ? ("col-sm-9") : ("")), 2 => (((twig_get_attribute($this->env, $this->source,         // line 29
($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 29) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 29)))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty(twig_get_attribute($this->env, $this->source,         // line 30
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 30)) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 30)))) ? ("col-sm-12") : (""))];
        // line 32
        echo "\t\t\t\t  ";
        // line 33
        echo "                ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 33)) {
            // line 34
            echo "                    ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 39
            echo "                ";
        }
        // line 40
        echo "                <section";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method", false, false, true, 40), 40, $this->source), "html", null, true);
        echo ">

                    ";
        // line 43
        echo "                    ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 43)) {
            // line 44
            echo "                        ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 47
            echo "                    ";
        }
        // line 48
        echo "
                    ";
        // line 50
        echo "                    ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "help", [], "any", false, false, true, 50)) {
            // line 51
            echo "                        ";
            $this->displayBlock('help', $context, $blocks);
            // line 54
            echo "                    ";
        }
        // line 55
        echo "
                    ";
        // line 57
        echo "                    ";
        $this->displayBlock('content', $context, $blocks);
        // line 61
        echo "                </section>

                ";
        // line 64
        echo "                ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 64)) {
            // line 65
            echo "                    ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 70
            echo "                ";
        }
        // line 71
        echo "            </div>
        </div>
    ";
    }

    // line 34
    public function block_sidebar_first($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "                        <aside class=\"col-sm-3 ssf-menu-lateral\" role=\"complementary\">
                            ";
        // line 36
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 36), 36, $this->source), "html", null, true);
        echo "
                        </aside>
                    ";
    }

    // line 44
    public function block_highlighted($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "                            <div class=\"highlighted\">";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 45), 45, $this->source), "html", null, true);
        echo "</div>
                        ";
    }

    // line 51
    public function block_help($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "                            ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "help", [], "any", false, false, true, 52), 52, $this->source), "html", null, true);
        echo "
                        ";
    }

    // line 57
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "                        <a id=\"main-content\"></a>
                        ";
        // line 59
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 59), 59, $this->source), "html", null, true);
        echo "
                    ";
    }

    // line 65
    public function block_sidebar_second($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 66
        echo "                        <aside class=\"col-sm-3\" role=\"complementary\">
                            ";
        // line 67
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 67), 67, $this->source), "html", null, true);
        echo "
                        </aside>
                    ";
    }

    // line 91
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 92
        echo "            <footer class=\"gov-co-footer\">
                ";
        // line 93
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 93), 93, $this->source), "html", null, true);
        echo "
                <div class=\"govco_bottom\">
                <div class=\"container\">
                    ";
        // line 96
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "govco_bottom", [], "any", false, false, true, 96), 96, $this->source), "html", null, true);
        echo "
                </div>
                </div>
            </footer>
    ";
    }

    // line 104
    public function block_copy($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 105
        echo "        <div class=\"copy\">
            <div class=\"container\" role=\"contentinfo\">
                ";
        // line 107
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "copy", [], "any", false, false, true, 107), 107, $this->source), "html", null, true);
        echo "
            </div>
        </div>
    ";
    }

    public function getTemplateName()
    {
        return "themes/custom/ssftheme/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  320 => 107,  316 => 105,  312 => 104,  303 => 96,  297 => 93,  294 => 92,  290 => 91,  283 => 67,  280 => 66,  276 => 65,  270 => 59,  267 => 58,  263 => 57,  256 => 52,  252 => 51,  245 => 45,  241 => 44,  234 => 36,  231 => 35,  227 => 34,  221 => 71,  218 => 70,  215 => 65,  212 => 64,  208 => 61,  205 => 57,  202 => 55,  199 => 54,  196 => 51,  193 => 50,  190 => 48,  187 => 47,  184 => 44,  181 => 43,  175 => 40,  172 => 39,  169 => 34,  166 => 33,  164 => 32,  162 => 30,  161 => 29,  160 => 28,  159 => 27,  157 => 26,  151 => 23,  147 => 22,  139 => 15,  135 => 13,  131 => 12,  123 => 6,  119 => 4,  115 => 3,  109 => 104,  107 => 103,  104 => 102,  100 => 91,  98 => 90,  90 => 85,  83 => 83,  75 => 78,  69 => 74,  66 => 22,  63 => 20,  59 => 12,  57 => 11,  53 => 3,  51 => 2,  49 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/ssftheme/templates/system/page.html.twig", "/var/www/drupal-9.3.0/themes/custom/ssftheme/templates/system/page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("include" => 1, "if" => 2, "block" => 3, "set" => 26);
        static $filters = array("escape" => 78);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if', 'block', 'set'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
