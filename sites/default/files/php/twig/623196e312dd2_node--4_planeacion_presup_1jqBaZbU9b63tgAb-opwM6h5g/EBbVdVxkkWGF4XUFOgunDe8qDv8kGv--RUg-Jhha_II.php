<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/ssftheme/templates/nodes/node--4_planeacion_presupuesto_e_infor.html.twig */
class __TwigTemplate_0ed25e7b511fbb0006ef185813f1055cdaf5e671a41699cfd8b05397616068c0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 3
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_descripcion", [], "any", false, false, true, 3), "value", [], "any", false, false, true, 3)) {
            // line 4
            echo "            <div class=\"node--ssf--descripcion\">
                ";
            // line 5
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_descripcion", [], "any", false, false, true, 5), 5, $this->source), "html", null, true);
            echo "
            </div>
        ";
        }
        // line 8
        echo "    </div>
    <div class=\"mt-4 mx-3 border shadow-sm p-3 mb-5 bg-white rounded\">
        <div>
            ";
        // line 11
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_fecha_generacion", [], "any", false, false, true, 11), "value", [], "any", false, false, true, 11)) {
            // line 12
            echo "                <div class=\"ssf--fecha1\">
                    ";
            // line 13
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_fecha_generacion", [], "any", false, false, true, 13), 13, $this->source), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 16
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_fecha_publicacion", [], "any", false, false, true, 16), "value", [], "any", false, false, true, 16)) {
            // line 17
            echo "                <div class=\"ssf--fecha2\">
                    ";
            // line 18
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_fecha_publicacion", [], "any", false, false, true, 18), 18, $this->source), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 21
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_clasificacion3", [], "any", false, false, true, 21), "value", [], "any", false, false, true, 21)) {
            // line 22
            echo "                <div class=\"ssf--clasificacion3\">
                    ";
            // line 23
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_clasificacion3", [], "any", false, false, true, 23), 23, $this->source), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 26
        echo "
            ";
        // line 27
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_text2", [], "any", false, false, true, 27), "value", [], "any", false, false, true, 27)) {
            // line 28
            echo "                <div class=\"ssf--text2\">
                    ";
            // line 29
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_text2", [], "any", false, false, true, 29), 29, $this->source), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 32
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_enlace", [], "any", false, false, true, 32), "value", [], "any", false, false, true, 32)) {
            // line 33
            echo "                <div class=\"ssf--enlace\">
                    ";
            // line 34
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_enlace", [], "any", false, false, true, 34), 34, $this->source), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 37
        echo "        </div>
        <div class=\"row\">
            <div class=\"col-md-12\">
                ";
        // line 40
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_varios_enlaces", [], "any", false, false, true, 40), "value", [], "any", false, false, true, 40)) {
            // line 41
            echo "                    <div class=\"ssf--variosenlaces\">
                        ";
            // line 42
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_varios_enlaces", [], "any", false, false, true, 42), 42, $this->source), "html", null, true);
            echo "
                    </div>
                ";
        }
        // line 45
        echo "            </div>
            <div class=\"col-md-12\">
                ";
        // line 47
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_archivo", [], "any", false, false, true, 47), "value", [], "any", false, false, true, 47)) {
            // line 48
            echo "                    <div class=\"ssf--adjuntos\">
                        ";
            // line 49
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_archivo", [], "any", false, false, true, 49), 49, $this->source), "html", null, true);
            echo "
                    </div>
                ";
        }
        // line 52
        echo "            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12\">
                ";
        // line 56
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_enlaces_relacionados", [], "any", false, false, true, 56), "value", [], "any", false, false, true, 56)) {
            // line 57
            echo "                    <div class=\"ssf--enlacesrelacionados\">
                        ";
            // line 58
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_enlaces_relacionados", [], "any", false, false, true, 58), 58, $this->source), "html", null, true);
            echo "
                    </div>
                ";
        }
        // line 61
        echo "            </div>
        </div>
    </div>
</div>
\t
\t


\t


\t\t
";
    }

    public function getTemplateName()
    {
        return "themes/custom/ssftheme/templates/nodes/node--4_planeacion_presupuesto_e_infor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 61,  162 => 58,  159 => 57,  157 => 56,  151 => 52,  145 => 49,  142 => 48,  140 => 47,  136 => 45,  130 => 42,  127 => 41,  125 => 40,  120 => 37,  114 => 34,  111 => 33,  108 => 32,  102 => 29,  99 => 28,  97 => 27,  94 => 26,  88 => 23,  85 => 22,  82 => 21,  76 => 18,  73 => 17,  70 => 16,  64 => 13,  61 => 12,  59 => 11,  54 => 8,  48 => 5,  45 => 4,  43 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/ssftheme/templates/nodes/node--4_planeacion_presupuesto_e_infor.html.twig", "/var/www/drupal-9.3.0/themes/custom/ssftheme/templates/nodes/node--4_planeacion_presupuesto_e_infor.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("if" => 3);
        static $filters = array("escape" => 5);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
